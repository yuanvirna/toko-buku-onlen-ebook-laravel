<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
|--------------------------------------------------------------------------
| Trash , Restore, Permanent Delete
|--------------------------------------------------------------------------
|
| Selalu letakkan 3 controller tersebut di atas resource method
| karena full memakai CRUD resource maka menggunakan ::resource
|
*/

Route::get('/', function(){
    return view('auth.login');
});
 /*
|--------------------------------------------------------------------------
| AUTH
|--------------------------------------------------------------------------
*/

Route::resource('/users',"UserController");
Auth::routes();//login

Route::match(["GET", "POST"], "/register", function(){
    return redirect("/login");
})->name("register");

Route::get('/home', 'HomeController@index')->name('home');
 /*
|--------------------------------------------------------------------------
| USERS
|--------------------------------------------------------------------------
*/

Route::resource('/users',"UserController");//karena full memakai resource

/*
|--------------------------------------------------------------------------
| CATEGORY
|--------------------------------------------------------------------------
*/

 Route::delete('/categories/{category}/delete-permanent', 'CategoryController@deletePermanent')->name('categories.delete-permanent');

 Route::get('/categories/{id}/restore', 'CategoryController@restore')->name('categories.restore');

 Route::get('/categories/trash', 'CategoryController@trash')->name('categories.trash');



Route::resource('/categories', 'CategoryController');
/*
|--------------------------------------------------------------------------
| BOOKS
|--------------------------------------------------------------------------
*/

Route::delete('/books/{id}/delete-permanent', 'BookController@deletePermanent')->name('books.delete-permanent');

Route::post('/books/{book}/restore', 'BookController@restore')->name('books.restore');

Route::get('/books/trash', 'BookController@trash')->name('books.trash');

Route::resource('/books', 'BookController');
Route::get('/ajax/categories/search', 'CategoryController@ajaxSearch'); // fitur pencarian buku menggunakan ajax
/*
|--------------------------------------------------------------------------
| ORDERS
|--------------------------------------------------------------------------
*/
Route::resource('orders', 'OrderController');