<?php

use App\User;
use Illuminate\Database\Seeder;

class AdministratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User;
        $admin->username = "admin";
        $admin->name = "Dashboard Admin";
        $admin->email = "admin@admin.test";
        $admin->roles = json_encode(["ADMIN"]);
        $admin->password = \Hash::make("rahasia123");
        $admin->avatar = "belum-ada-file";
        $admin->address = "Jalan kenang kita";
        $admin->phone = "082241696397";
        $admin->status = "ACTIVE";

        $admin->save();

        $this->command->info("User admin berhasil di masukkan");
    }
}