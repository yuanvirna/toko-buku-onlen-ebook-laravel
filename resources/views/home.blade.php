<!-- untuk dapat melihat tampilan yang ada di global 
    maka gunakan  @extends('layouts.global')
 -->

@extends('layouts.global')
<!-- ini untuk menambah title  -->
@section("title") | Home @endsection

<!-- 

    kita bisa melihat tampilan "You are logged in" itu karena di view 
    home menggunakan slot @content dengan kode berikut:
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

 -->
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <!-- ini adalah kondisi status apa saja yang akan 
                    ditampilkan dari child-->
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection