@extends('layouts.global')

@section('title') Tambah book @endsection

@section('content')
<!-- NOTIF -->
@if(session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
@endif
<!-- FORM -->
<div class="row">
    <div class="col-md-8">
        <form action="{{route('books.store')}}" method="POST" enctype="multipart/form-data"
            class="shadow-sm p-3 bg-white">

            @csrf

            <label for="title">Title</label> <br>
            <input value="{{old('title')}}" type="text"
                class="form-control {{$errors->first('title') ? "is-invalid" : ""}} " name="title"
                placeholder="Book title">
            <div class="invalid-feedback">
                {{$errors->first('title')}}
            </div>
            <br>

            <label for="cover">Cover</label>
            <input type="file" class="form-control {{$errors->first('cover') ? "is-invalid" : ""}} " name="cover">
            <div class="invalid-feedback">
                {{$errors->first('cover')}}
            </div>
            <br>

            <label for="description">Description</label><br>
            <textarea name="description" id="description"
                class="form-control {{$errors->first('description') ? "is-invalid" : ""}} "
                placeholder="Give a description about this book">{{old('description')}}</textarea>
            <div class="invalid-feedback">
                {{$errors->first('description')}}
            </div>
            <br>

            <!-- SELECT CATEGORIES -->
            <label for="categories">Categories</label>
            <br>
            <select name="categories[]" multiple id="categories" class="form-control">
            </select>
            <br>
            <br>
            <!-- 

                Input untuk memilih kategori di atas merupakan select karena input tersebut 
                yang akan kita pasangkan dengan plugin select2 kemudian kita beri attribute 
                multiple dan name menggunakan array ([]) seperti ini categories[] karena 
                kita akan mengizinkan lebih dari satu kategori dipilih. Dan kita beri id 
                categories sebagai selector untuk select2.

             -->

            <label for="stock">Stock</label><br>
            <input value="{{old('stock')}}" type="number"
                class="form-control {{$errors->first('stock') ? "is-invalid" : ""}} " id="stock" name="stock" min=0
                value=0>
            <div class="invalid-feedback">
                {{$errors->first('stock')}}
            </div>
            <br>

            <label for="author">Author</label><br>
            <input value="{{old('author')}}" type="text"
                class="form-control {{$errors->first('author') ? "is-invalid" : ""}} " name="author" id="author"
                placeholder="Book author">
            <div class="invalid-feedback">
                {{$errors->first('author')}}
            </div>
            <br>

            <label for="publisher">Publisher</label> <br>
            <input value="{{old('publisher')}}" type="text"
                class="form-control {{$errors->first('publisher') ? "is-invalid" : ""}} " id="publisher"
                name="publisher" placeholder="Book publisher">
            <div class="invalid-feedback">
                {{$errors->first('publisher')}}
            </div>
            <br>

            <label for="Price">Price</label> <br>
            <input value="{{old('price')}}" type="number"
                class="form-control {{$errors->first('price') ? "is-invalid" : ""}} " name="price" id="price"
                placeholder="Book price">
            <div class="invalid-feedback">
                {{$errors->first('price')}}
            </div>
            <br>
            <!-- BUTTON PUBLISH -->
            <!-- untuk langsung release -->
            <button class="btn btn-primary" name="save_action" value="PUBLISH">Publish</button>
            <!-- BUTTON SAVE TO DRAFT -->
            <!-- untuk di simpan sementara -->
            <button class="btn btn-secondary" name="save_action" value="DRAFT">Save as draft</button>
        </form>
    </div>
</div>
@endsection

@section('footer-scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<!--  kode jquery select2 -->
<script>
$('#categories').select2({
    ajax: {
        url: 'http://127.0.0.1:8000/ajax/categories/search',
        processResults: function(data) {
            return {
                results: data.map(function(item) {
                    return {
                        id: item.id,
                        text: item.name
                    }
                })
            }
        }
    }
});
</script>

@endsection