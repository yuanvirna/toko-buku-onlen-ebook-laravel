@extends("layouts.global")

@section("title") Daftar User @endsection

<!-- @section("content") -->

<!-- gunakan foreach untuk list data user dari variabel
     yang ada di controller -->
@section("content")

<!-- NOTIF -->
@if(session('status'))
<div class="alert alert-success">
    {{session('status')}}
</div>
@endif

<!-- FILTER -->

<form action="{{route('users.index')}}">
    <div class="row">
        <div class="col-md-5">
            <input value="{{Request::get('keyword')}}" name="keyword" class="form-control" type="text"
                placeholder="Masukkan kata kunci.." />
        </div>
        <div class="row">
            <div class="col-md-3">
                <input {{Request::get('status') == 'ACTIVE' ? 'checked' : ''}} value="ACTIVE" name="status" type="radio"
                    class="form-control" id="active">
                <label for="active">Active</label>
            </div>
            <div class="col-md-3">
                <input {{Request::get('status') == 'INACTIVE' ? 'checked' : ''}} value="INACTIVE" name="status"
                    type="radio" class="form-control" id="inactive">
                <label for="inactive">Inactive</label>
            </div>
            <div class="col-md-2">
                <input type="submit" value="Filter" class="btn btn-primary">
            </div>
        </div>


    </div>
</form>

<hr class="my-2">

<!-- meletakkan daftar user pada section content -->
<!-- FITUR CREATE USER -->

<div class="row">
    <div class="col-md-12 text-left">
        <a href="{{route('users.create')}}" class="btn btn-primary btn-sm">+ Tambah user</a>
    </div>
</div>
<br />
<table class="table table-bordered">
    <thead>
        <tr>
            <th><b>Name</b></th>
            <th><b>Username</b></th>
            <th><b>Email</b></th>
            <th><b>Avatar</b></th>
            <th><b>Status</b></th>
            <th><b>Action</b></th>

        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <!-- mengambil berdasarkan row atau data -->
            <!-- NAME -->
            <td>{{$user->name}}</td>
            <!-- USERNAME -->
            <td>{{$user->username}}</td>
            <!-- EMAIL -->
            <td>{{$user->email}}</td>
            <!-- AVATAR -->
            <td>
                <!-- untuk avatar terdapat 2 state
                    1. jika ada avatarnya
                    2. jika tidak ada 
                -->
                @if($user->avatar)
                <img src="{{asset('storage/'.$user->avatar)}}" width="70px" />
                @else
                N/A
                @endif

            </td>
            <!-- STATUS -->
            <td>
                @if($user->status == "ACTIVE")
                <span class="badge badge-success">
                    {{$user->status}}
                </span>
                @else
                <span class="badge badge-danger">
                    {{$user->status}}
                </span>
                @endif
            </td>
            <!-- ACTION -->
            <td>
                <!-- [TODO: actions] -->
                <!-- EDIT -->
                <a class="btn btn-info text-white btn-sm" href="{{route('users.edit', [$user->id])}}">
                    Edit</a>
                <!-- DETAIL -->
                <a href="{{route('users.show', [$user->id])}}" class="btn btn-primary btn-sm">Detail</a>
                <!-- DELETE -->

                <!-- Set validasi apakah benar ingin dihapus -->
                <form onsubmit="return confirm('Delete this user permanently?')" class="d-inline"
                    action="{{route('users.destroy', [$user->id])}}" method="POST">

                    @csrf

                    <input type="hidden" name="_method" value="DELETE">

                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan=10>
                {{$users->appends(Request::all())->links()}}
            </td>
        </tr>
    </tfoot>
</table>

@endsection