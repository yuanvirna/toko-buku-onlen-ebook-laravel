@extends("layouts.global")

@section("title") Tambahkan User Baru @endsection

@section("content")

<div class="col-md-8">
    <!-- notif for successfully adding new user -->
    @if(session('status'))
    <!--nge check apakah ada variable status -->
    <div class="alert alert-success">
        <!-- jika ada akan di tampilan alert -->
        {{session('status')}}
    </div>
    @endif
    <!-- buat form -->
    <!-- kita juga set form attribute enctype bernilai 
        "multipart/form-data" 
        karena kita akan mengunggah (mengupload) file dari form ini. -->
    <form enctype="multipart/form-data" class="bg-white shadow-sm p-3" action="{{route('users.store')}}" method="POST">

        @csrf
        <!-- blade helper @csrf ini berfungsi 
        untuk menggenerate token 
        csrf seperti ini: -->
        <!-- 
        <input 
         type="hidden"  
       (
            hidden fungsinya untuk ngecheck bahwa request 
            yang dikirimkan adalah request yang valid. 
       ) 
        name="_token"
        value="HPoyywkM2LR0RMlL4qHfe9TwPAi75IgvXeL9PX4b">
         -->
        <!-- INPUT WITH VALIDATION -->
        <!-- FIELD NAME -->
        <label for="name">Nama</label>
        <!-- 
            1. menampilkan data yang diketik oleh user sebelumnya menggunakan method old('name')
            2. berikan class is-invalid jika terdapat error untuk field name. Untuk mengecek apakah 
               terdapat error kita bisa gunakan $errors->first('name')
            3. Pemberian class "is-invalid" adalah agar input tersebut memiliki border berwarna merah. 
               Ini merupakan styling / class dari template bootstrap yang kita pakai, yaitu polished template.
         -->
        <input value="{{old('name')}}" class="form-control {{$errors->first('name') ? "is-invalid": ""}}"
            placeholder="Full Name" type="text" name="name" id="name" />
        <!-- 4. tampilkan pesan error dari field name -->
        <div class="invalid-feedback">
            {{$errors->first('name')}}
        </div>
        <br>
        <!-- FIELD USERNAME -->
        <label for="username">Username</label>
        <input value="{{old('username')}}" class="form-control {{$errors->first('username') ? "is-invalid" : ""}}"
            placeholder="username" type="text" name="username" id="username" />
        <div class="invalid-feedback">
            {{$errors->first('username')}}
        </div>
        <br>
        <!-- FIELD ROLES -->
        <label for="">Roles</label>
        <br>
        <!-- ADMIN  -->
        <input class="form-control {{$errors->first('roles') ? "is-invalid" : "" }}" type="checkbox" name="roles[]"
            id="ADMIN" value="ADMIN">
        <label for="ADMIN">Administrator</label>
        <!-- STAFF -->
        <input class="form-control {{$errors->first('roles') ? "is-invalid" : "" }}" type="checkbox" name="roles[]"
            id="STAFF" v alue="STAFF">
        <label for="STAFF">Staff</label>
        <!-- CUSTOMER -->
        <input class="form-control {{$errors->first('roles') ? "is-invalid" : "" }}" type="checkbox" name="roles[]"
            id="CUSTOMER" value="CUSTOMER">
        <label for="CUSTOMER">Customer</label>
        <div class="invalid-feedback">
            {{$errors->first('roles')}}
        </div>
        <br>
        <!-- FIELD NOMOR HP -->
        <br>
        <label for="phone">Nomor Hp</label>
        <br>
        <input value="{{old('phone')}}" type="text" name="phone"
            class="form-control {{$errors->first('phone') ? "is-invalid" : ""}} ">
        <div class="invalid-feedback">
            {{$errors->first('phone')}}
        </div>
        <br>
        <!-- FIELD ADDRESS -->
        <label for="address">Alamat</label>
        <textarea name="address" id="address" class="form-control {{$errors->first('address') ? "is-invalid" : ""}}">
            {{old('address')}}
            </textarea>
        <div class="invalid-feedback">
            {{$errors->first('address')}}
        </div>
        <br>
        <!-- FIELD AVATAR -->
        <label for="avatar">Avatar</label>
        <br>
        <input id="avatar" name="avatar" type="file"
            class="form-control {{$errors->first('avatar') ? "is-invalid" : ""}}">
        <div class="invalid-feedback">
            {{$errors->first('avatar')}}
        </div>

        <hr class="my-3">
        <!-- FIELD EMAIL -->
        <label for="email">Email</label>
        <input value="{{old('email')}}" class="form-control {{$errors->first('email') ? "is-invalid" : ""}}"
            placeholder="user@mail.com" type="text" name="email" id="email" />
        <div class="invalid-feedback">
            {{$errors->first('email')}}
        </div>
        <br>
        <!-- FIELD PASSWORD -->
        <label for="password">Password</label>
        <input class="form-control {{$errors->first('password') ? "is-invalid" : ""}}" placeholder="password"
            type="password" name="password" id="password" />
        <div class="invalid-feedback">
            {{$errors->first('password')}}
        </div>
        <br>
        <!-- FIELD PASSWORD CONFIRM -->
        <label for="password_confirmation">Konfirmasi Password</label>
        <input class="form-control {{$errors->first('password_confirmation') ? "is-invalid" : ""}}"
            placeholder="password confirmation" type="password" name="password_confirmation"
            id="password_confirmation" />
        <div class="invalid-feedback">
            {{$errors->first('password_confirmation')}}
        </div>
        <br>

        <input class="btn btn-primary" type="submit" value="Save" />
    </form>
</div>

@endsection