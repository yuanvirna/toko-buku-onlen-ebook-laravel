@extends('layouts.global')

@section('title') Edit User @endsection

<!-- buat form -->
@section('content')
<div class="col-md-8">

    @if(session('status'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
    @endif

    <form enctype="multipart/form-data" class="bg-white shadow-sm p-3" action="{{route('users.update', [$user->id])}}"
        method="POST">

        @csrf
        <!-- 
            Untuk menggunakan method selain GET dan POST,maka kita 
            harus memberikan nilai method pada form sebagai POST, 
            lalu kita tambahkan input bertipe hidden dengan 
            nama _method bernilai nama method yang akan 
            digunakan. 
            Misalnya untuk mengirim data dengan method PUT kita 
            menggunakan input hidden ini:
            <input name="_method" name="PUT">
            Dan untuk menggunakan method DELETE tinggal kita 
            ganti valuenya menjadi seperti ini:
            <input name="_method" name="DELETE">
         -->
        <!-- INPUT WITH VALIDATION -->
        <input type="hidden" value="PUT" name="_method">
        <!-- NAME -->
        <label for="name">Name</label>
        <label for="name">Name</label>
        <input value="{{old('name') ? old('name') : $user->name}}"
            class="form-control {{$errors->first('name') ? "is-invalid" : ""}}" placeholder="Full Name" type="text"
            name="name" id="name" />
        <div class="invalid-feedback">
            {{$errors->first('name')}}
        </div>
        <br>
        <!-- USERNAME -->
        <!-- agar tidak bisa diubah gunakan disabled -->
        <label for="username">Username</label>
        <input value="{{$user->username}}" disabled class="form-control" placeholder="username" type="text"
            name="username" id="username" />
        <br>
        <!-- FITUR STATUS -->
        <label for="">Status</label>
        <br />
        <input {{$user->status == "ACTIVE" ? "checked" : ""}} value="ACTIVE" type="radio" class="form-control"
            id="active" name="status"> <label for="active">Active</label>
        <input {{$user->status == "INACTIVE" ? "checked" : ""}} value="INACTIVE" type="radio" class="form-control"
            id="inactive" name="status"> <label for="inactive">Inactive</label>
        <br><br>
        <!-- ROLES -->
        <label for="">Roles</label>
        <!-- <br> -->
        <!-- 
            Untuk roles kita akan mencentang input 
            checkbox di tiap-tiap role yang dimiliki user, 
            kita melakukannya bantuan php function in_array()
         -->
        <!-- 
             Kode di atas mengecek apakah $user->roles didalamnya 
             terdapat string "ADMIN". Kita menggunakan json_decode() 
             untuk mengubah dari string JSON Array kembali ke PHP Array.
          -->
        <!-- ADMIN -->
        <br>
        <input type="checkbox" {{in_array("ADMIN", json_decode($user->roles)) ? "checked" : ""}} name="roles[]"
            class="form-control {{$errors->first('roles') ? "is-invalid" : "" }}" id="ADMIN" value="ADMIN">
        <label for="ADMIN">Administrator</label>
        <!-- STAFF -->
        <input type="checkbox" {{in_array("STAFF", json_decode($user->roles)) ? "checked" : ""}} name="roles[]"
            class="form-control {{$errors->first('roles') ? "is-invalid" : "" }}" id="STAFF" value="STAFF">
        <label for="STAFF">Staff</label>
        <!-- CUSTOMER -->
        <input type="checkbox" {{in_array("CUSTOMER", json_decode($user->roles)) ? "checked" : ""}} name="roles[]"
            class="form-control {{$errors->first('roles') ? "is-invalid" : "" }}" id="CUSTOMER" value="CUSTOMER">
        <label for="CUSTOMER">Customer</label>
        <div class="invalid-feedback">
            {{$errors->first('roles')}}
        </div>
        <br>

        <br>
        <!-- PHONE -->
        <label for="phone">Phone number</label>
        <br>
        <input type="text" name="phone" class="form-control {{$errors->first('phone') ? "is-invalid" : ""}}"
            value="{{old('phone') ? old('phone') : $user->phone}}">
        <div class="invalid-feedback">
            {{$errors->first('phone')}}
        </div>

        <br>
        <!-- ADDRESS -->
        <label for="address">Address</label>
        <textarea name="address" id="address"
            class="form-control {{$errors->first('address') ? "is-invalid" : ""}}">{{old('address') ? old('address') : $user->address}}</textarea>
        <div class="invalid-feedback">
            {{$errors->first('address')}}
        </div>
        <br>
        <!-- AVATAR -->
        <label for="avatar">Avatar image</label>
        <br>
        <!-- AVATAR -->
        <!-- 
            Khusus untuk menampilkan avatar kita lakukan pengecekkan, 
            jika user memiliki avatar kita tampilkan image avatarnya, 
            jika tidak ada avatar kita tampilkan teks "Tidak ada avatar"
         -->
        Current avatar: <br>
        @if($user->avatar)
        <img src="{{asset('storage/'.$user->avatar)}}" width="120px" />
        <br>
        @else
        No avatar
        @endif
        <br>
        <input id="avatar" name="avatar" type="file" class="form-control">
        <!-- 
            keterangan untuk mengabaikan field input ini 
            jika tidak ingin mengubah avatar 
        -->
        <small class="text-muted">Kosongkan jika tidak ingin mengubah avatar</small>

        <hr class="my-3">
        <!-- EMAIL -->
        <label for="email">Email</label>
        <input value="{{$user->email}}" disabled class="form-control {{$errors->first('email') ? "is-invalid" : ""}} "
            placeholder="user@mail.com" type="text" name="email" id="email" />
        <div class="invalid-feedback">
            {{$errors->first('email')}}
        </div>
        <br>
        <input class="btn btn-primary" type="submit" value="Save" />
    </form>
</div>
@endsection