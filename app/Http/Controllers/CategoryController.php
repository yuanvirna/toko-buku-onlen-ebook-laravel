<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Gate;

class CategoryController extends Controller
{
    public function __construct()
    {
        // OTORISASI GATE
        $this->middleware(function($request, $next)
        {
            if(Gate::allows('manage-categories')) return $next($request);
            abort(403, 'Oops!Anda tidak memiliki hak akses kesini');
          });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //get all data categories
        $categories = Category::paginate(10);
        // fitur search
        $filterKeyword = $request->get('name');
        // cek jika $filterKeyword memiliki nilai maka kita gunakan variable 
        // tersebut untuk memfilter model Category yang akan kita lempar ke view
        if($filterKeyword)
        {
            $categories = Category::where("name","LIKE","%$filterKeyword%")->paginate(10);
        }
        return view('categories.index',['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(),[
            "name" => "required|min:2|max:15",
            "image" => "required"
        ])->validate();
        $name = $request->get('name');

        $category = new Category;
        $category->name = $name;
        // menangkap request dengan nama 'name' ke dalam variabel $name 
        // kemudian membuat sebuah instance dari model Category baru 
        // dan memberikan nilai name = $name
        if($request->file('image'))//mencari request bertipe file() dengan nama ('image')
        {
            $path = $request->file('image')->store('image_categories', 'public');//simpan file tersebut ke folder storage/app/public/image_categories

            $category->image = $path;//menyimpan path gambar ke folder public
        }
        $category->created_by = \Auth::user()->id; // mengindikasi siapa yang buat category 
            // dengan mengambil id user kemudian menyimpannya di field created_by
        $category->slug = \Str::slug($name,'-'); // generate slug dengan helper Str -> string
        // ----------------slug-----------
        //Slug merupakan karakter yang tidak menyalahi aturan URL, artinya tidak ada spasi, 
        // huruf besar kecil, dsb. Misalnya nama kategori "Sepatu Olahraga" maka dengan 
        // \Str::slug("Sepatu Olahraga", "-") akan menghasilkan "sepatu-olahraga"
        $category->save();
        return redirect()->route('categories.index')->with('status', 'Category successfully created');//notif

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);
        return view('categories.show', ['category' => $category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);

        return view('categories.edit', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //cari kategori yang akan di edit
        $category = Category::findOrFail($id);
        // untuk slug, dibuat validasinya unique 
        // tapi terkecualikan nilai yang sekarang ada di databae
        // Jika hanya menggunakan unique:categories maka kita akan 
        // menemui error ketika akan mengupdate category jika tidak 
        // mengubah slug,  karena slug sudah ada di database,yang sebetulnya 
        // adalah slug dari kategori yang diedit tersebut. Untuk itu kita perlu 
        // mengabaikan nilai dari slug yang sekarang dimiliki oleh kategori yang diupdate. 
        // Caranya menggunakan Illuminate\Validation\Rule.
        \Validator::make($request->all(),[
            "name" => "required|min:3|max:15",
            "image" => "required",
            "slug" => [
              "required",
              Rule::unique("categories")->ignore($category->slug, "slug")
            ]
        ])->validate();
        //menangkap request
        $name = $request->get('name');
        $slug = $request->get('slug');
        
        //field-field yang diedit dengan nilai dari request yang kita tangkap
        $category->name = $name;
        $category->slug = $slug;
        if($request->file('image')){
            if($category->image && file_exists(storage_path('app/public/' . $category->image)))
            {
                \Storage::delete('public/' . $category->name);
            }
    
            $image = $request->file('image')->store('category_images', 'public');
    
            $category->image = $image;
        }
        $category->updated_by = \Auth::user()->id;
        $category->slug = \Str::slug($name);
    
        $category->save();
    
        return redirect()->route('categories.index', [$id])->with('status', 'Category succesfully update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        return redirect()->route('categories.index')
        ->with('status', 'Category successfully moved to trash');
    }
    public function trash()
    {
        $deleted_category = Category::onlyTrashed()->paginate(10);

        return view('categories.trash', ['categories' => $deleted_category]);
    }
    public function restore($id)
    {
        //mencari semua Category baik yang aktif maupun yang ada di trash / soft deleted.
        $category = Category::withTrashed()->findOrFail($id);
        //Jika tidak menggunakan withTrashed() akan sangat mungkin kita mendapatkan error not found.

        if($category->trashed())
        {
            $category->restore();//helper restore data
        } else {
            return redirect()->route('categories.index')->with('status','Category tidak ada');
        }
        return redirect()->route('categories.index')->with('status','Category terbuang');
    }
    public function permanentDeleted()
    {
        //mencari semua Category baik yang aktif maupun yang ada di trash / soft deleted.
        $category = Category::withTrashed()->findOrFail($id);
        //Jika tidak menggunakan withTrashed() akan sangat mungkin kita mendapatkan error not found.

        if($category->trashed())
        {
            return redirect()->route('categories.index')->with('status','Category aktif tidak bisa di hapus permanent');
        } else {
           $category->forceDelte();//helper delete permanent
        }
        return redirect()->route('categories.index')->with('status','Category terbuang permanent');   
    }
    public function ajaxSearch(Request $request){
        $keyword = $request->get('q');
       
        $categories = Category::where("name", "LIKE", "%$keyword%")->get();
       
        return $categories;
       }
   
}