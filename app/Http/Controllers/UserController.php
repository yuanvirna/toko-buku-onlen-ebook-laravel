<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        // OTORISASI GATE
        $this->middleware(function($request, $next)
        {
            if(Gate::allows('manage-users')) return $next($request);
            abort(403, 'Oops!Anda tidak memiliki hak akses kesini');
          });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // get all data with paginate
        $users = User::paginate(10);
        // FILTER
        // ambil request inputtan
        $filterKeyword = $request->get('keyword');
        // cek jika ada $filterKeyword maka kita query User 
        // yang emailnya memiliki sebagian dari keyword 
        $status = $request->get('status');
        if($status)
        {
                $users = User::where('status', $status)->paginate(10);
        } else {
                $users = User::paginate(10);
        }
        if($filterKeyword)
        {
            // kita cari menggunakan model User method where(),dan kita 
            // gunakan operator LIKE dan nilai dari keyword 
            // kita bungkus dalam '%%' sehingga query 
            // akan mencari User yang memiliki 
            // email mengandung keyword tertentu, 
            // bukan exact match
            if($status){
                $users = \App\User::where('email', 'LIKE', "%$filterKeyword%")
                    ->where('status', $status)
                    ->paginate(10);
            } else {
                $users = \App\User::where('email', 'LIKE', "%$filterKeyword%")
                    ->paginate(10);
            }
         }
        // return view to list all user
        return view('users.index',['users' => $users]);
        // untuk test form -> return view("users.create");
        //ini fitur form untuk membuat user
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("users.create");
        //ini fitur form untuk membuat user
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // membuat validation menggunakan facade \Validator dengan method make()
        // Method tersebut menerima 2 parameter, parameter pertama kita isi dengan semua request dari form $request->all() 
        // bertipe array, parameter kedua juga bertipe array, yaitu rules dari masing-masing field yang divalidas
        \Validator::make($request->all(),[
            "name" => "required|min:4|max:70",
            "username" => "required|min:2|max:40",
            "roles" => "required",
            "avatar" => "required",
            "address" => "required|min:8",
            "email" => "required|email",
            "password" => "required|min:8",
            "password_confirmation" => "required|min:8|same:password" 
        ])->validate();
        // setelah membuat validator nya , di view juga dibuat untuk memunculkan tampilan / notif errornya
        $create_user = new User;
        $create_user->username = $request->get('name');
        $create_user->name= $request->get('username');
        $create_user->roles = json_encode($request->get('roles'));
        $create_user->phone = $request->get('phone');
        $create_user->address = $request->get('address');
        $create_user->email = $request->get('email');
        $create_user->password = \Hash::make($request->get('password'));
        // upload file handling
        if($request->file('avatar'))
        // cek apakah request memiliki file dengan nama avatar,
        // jika ada maka simpan file tersebut menggunakan method 
        // $request->file()->store().
        {
            $file = $request->file('avatar')->store('avatars', 'public');
            $create_user->avatar = $file; 
            // Method store() akan menghasilkan path dari file yang kita simpan. 
            // Sehingga kode di atas akan menghasilkan string path di mana lokasi file berada, 
            // kita memerlukan string tersebut untuk disimpan sebagai nilai 'avatar' di kolom tabel users
        }
        $create_user->save();
        // send notif for success adding new user
        return redirect()->route('users.index')->with('status', 'User successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id); //cari ID
        return view('users.detail', ['user' => $user]); //tampilkan 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        //gunakan findOrFail agar jika id user atau user 
        //tidak ditemukan , akan redirect ke page not found, 
        //dan bukan error
        return view('users.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \Validator::make($request->all(),[
            "name" => "required|min:4|max:70",
            "roles" => "required",
            "phone" => "required|digits_between:10,12",
            "address" => "required|min:10"
        ])->validate();
        $user = User::findOrFail($id);

        $user->name = $request->get('name');
        $user->address = $request->get('address');
        $user->roles = json_encode($request->get('roles'));
         // Untuk field roles sebelum menyimpan ke database 
        // kita ubah dulu dari PHP Array menjadi JSON 
        // Array menggunakan fungsi json_encode() 
        // agar dapat disimpan ke database.
        $user->phone = $request->get('phone');
        $user->status = $request->get('status');
        if($request->file('avatar')) 
        // cek apakah ada request bertipe file dengan nama 'avatar'
        {
            // jika ada , kita cek lagi
            if($user->avatar && file_exists(storage_path('app/public/' . $user->avatar)))
            {
                //  apakah user yang akan diedit ini memiliki file avatar 
                // dan apakah file tersebut ada di database, jika ada 
                // maka kita hapus file tersebut
                \Storage::delete('public/'.$user->avatar);
                // Untuk menghapus file kita gunakan \Storage::delete() argument yang 
                // dimasukan adalah path relatif dari storage/app tidak perlu 
                // full path. Maka dari itu kita menggunakan 
                // 'public/'. $user->avatar yang akan menghapus file di 
                // folder storage/app/public/avatars/fileavataruser.png misalnya.
            }
            // Setelah itu kita simpan file yang diupload ke folder "avatars" 
            // seperti sebelumnya menggunakan method store()
            $file = $request->file('avatar')->store('avatars', 'public');
            // Lalu kita set field 'avatar' user dengan path baru dari image 
            // yang diupload tadi
            $user->avatar = $file;
        }
        $user->save();
        //redirect ke form edit user dengan status bahwa berhasil melakukan update
        return redirect()->route('users.index')->with('status', 'User succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('users.index')->with('status', 'User successfully deleted');
    }
}