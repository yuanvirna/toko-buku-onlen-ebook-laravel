<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Gate;

class BookController extends Controller
{
    public function __construct()
    {
        // OTORISASI GATE
        $this->middleware(function($request, $next)
        {
            if(Gate::allows('manage-books')) return $next($request);
            abort(403, 'Oops!Anda tidak memiliki hak akses kesini');
          });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // memberikan query param / query string di url seperti ini:

        // http://{url}/books?status=publish

        // Untuk menampilkan buku berstatus PUBLISH

        // http://{url}/books?status=draft

        // Untuk menampilkan buku berstatus DRAFT
        $status = $request->get('status');
        $keyword = $request->get('keyword') ? $request->get('keyword') : '';
    
        if($status){
            $books = \App\Book::with('categories')->where('title', "LIKE", "%$keyword%")->where('status', strtoupper($status))->paginate(10);
        } else {
            $books = \App\Book::with('categories')->where("title", "LIKE", "%$keyword%")->paginate(10);
        }
    
        return view('books.index', ['books' => $books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(),[
            "title" => "required|min:5|max:200",
            "description" => "required|min:20|max:1000",
            "author" => "required|min:3|max:100",
            "publisher" => "required|min:3|max:200",
            "price" => "required|digits_between:0,10",
            "stock" => "required|digits_between:0,10",
            "cover" => "required"
        ])->validate();
        $books = new Book;
        $books->title = $request->get('title');
        $books->description = $request->get('description');
        $books->author = $request->get('author');
        $books->publisher = $request->get('publisher');
        $books->price = $request->get('price');
        $books->stock = $request->get('stock');

        $books->status = $request->get('save_action');//save_action = tipe button
        // nilainya hanya dua yaitu PUBLISH atau DRAFT sehingga keduanya 
        // sudah sesuai dengan tipe field status pada tabel books

        $cover = $request->file('cover');
        if($cover){
        $cover_path = $cover->store('book-covers', 'public');

        $books->cover = $cover_path;
        }
        $books->slug = \Str::slug($request->get('title'));
        $books->created_by = \Auth::user()->id; 
        $books->save();
        $books->categories()->attach($request->get('categories'));//dapatkan kategori
        // kondisi ketika akan menyimpan
        if($request->get('save_action') == 'PUBLISH')
        {
            // jika ingin di publish akan notifnya seperti ii
            return redirect()->route('books.index')->with('status', 'Book successfully saved and published');
        } else 
        {
            // jika ingin di simpan dulu di draft notifnya seperti ini
            return redirect()->route('books.index')->with('status', 'Book saved as draft');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::findOrFail($id);
        return view('books.edit', ['book' => $book]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::findOrFail($id);
        \Validator::make($request->all(),[
            "title" => "required|min:5|max:200",
            "slug" => [
            "required",
                Rule::unique("books")->ignore($book->slug, "slug")
                    ],
            "description" => "required|min:7|max:1000",
            "author" => "required|min:3|max:100",
            "publisher" => "required|min:3|max:200",
            "price" => "integer|required|digits_between:0,10",
            "stock" => "required|digits_between:0,10",
        ])->validate();

        $book->title = $request->get('title');
        $book->slug = $request->get('slug');
        $book->description = $request->get('description');
        $book->author = $request->get('author');
        $book->publisher = $request->get('publisher');
        $book->stock = $request->get('stock');
        $book->price = $request->get('price');

        $new_cover = $request->file('cover');

        if($new_cover){
        if($book->cover && file_exists(storage_path('app/public/' . $book->cover))){
            \Storage::delete('public/'. $book->cover);
        }

        $new_cover_path = $new_cover->store('book-covers', 'public');

        $book->cover = $new_cover_path;
    }

    $book->updated_by = \Auth::user()->id;

    $book->status = $request->get('status');

    $book->categories()->sync($request->get('categories'));

    $book->save();

    return redirect()->route('books.index')->with('status', 'Book successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = \App\Book::findOrFail($id);
        $book->delete();

        return redirect()->route('books.index')->with('status', 'Book moved to trash');
    }
    public function trash()
    {   
        $books = \App\Book::onlyTrashed()->paginate(10);

        return view('books.trash', ['books' => $books]);

    }
    public function restore($id)
    {
        $book = \App\Book::withTrashed()->findOrFail($id);
      
        if($book->trashed()){
          $book->restore();
          return redirect()->route('books.trash')->with('status', 'Book successfully restored');
        } else {
          return redirect()->route('books.trash')->with('status', 'Book is not in trash');
        }
    }
    public function deletePermanent($id)
    {
        $book = \App\Book::withTrashed()->findOrFail($id);
      
        if(!$book->trashed()){
          return redirect()->route('books.trash')->with('status', 'Book is not in trash!')->with('status_type', 'alert');
        } else {
          $book->categories()->detach();
          $book->forceDelete();
      
          return redirect()->route('books.trash')->with('status', 'Book permanently deleted!');
        }
    }
}