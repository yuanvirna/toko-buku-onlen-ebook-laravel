<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->registerPolicies();

        Gate::define('manage-users', function($user){
       //  gate untuk mengizinkan role admin memanage user
       return count(array_intersect(["ADMIN"], json_decode($user->roles)));
        });

        Gate::define('manage-categories', function($user){
       //  gate untuk mengizinkan role admin dan atau staff mengelola category
       return count(array_intersect(["ADMIN", "STAFF"], json_decode($user->roles)));
        });

        Gate::define('manage-books', function($user){
        // gate untuk mengizinkan role admin dan atau staf mengelola produk buku
        return count(array_intersect(["ADMIN", "STAFF"], json_decode($user->roles)));
        });

        Gate::define('manage-orders', function($user){
       //  gate untuk mengizinkan role admin dan atau staff mengelola order
        return count(array_intersect(["ADMIN", "STAFF"], json_decode($user->roles)));
        });

        // Catatan: Kita menggunakan json_decode() pada $user->roles karena 
        // $user->roles bertipe JSON String array, sehingga kita perlu menggunakan 
        // json_decode() untuk mengubahnya menjadi PHP Array.
    }
}