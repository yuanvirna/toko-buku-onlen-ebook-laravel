<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;
    public function categories()
    {
        return $this->belongsToMany('App\Category');
      }
    public function books()
    {
        return $this->belongsToMany('App\Book')->withPivot('quantity');
        //mengambil field yang berada di tabel pivot yaitu field quantity
    }
    public function orders()
    {
      return $this->belongsToMany('App\Order');
    }
}