<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function books()
    {
        return $this->belongsToMany('App\Book')->withPivot('quantity');
    }
    // dynamic property di model Order 
    public function getTotalQuantityAttribute()
    {
        $total_quantity = 0;

        foreach($this->books as $book){
            $total_quantity += $book->pivot->quantity; 
            // menjumlahkan quantity yang diambil dari tabel pivot, 
            // hasil penjumlahan tersebut yang akan menjadi nilai dari dynamic property
        }

        return $total_quantity;
    }
}